# Forkio Project

## Description

Landing page for Forkio project (with adaptive & responsive markup)

See https://gitlab.com/dan-it/groups/pe35/-/tree/master/advanced-html-css/step-project-forkio for more details.

# Technologies we used:
Gulp:
-devDependencies:
"autoprefixer",
"browser-sync",
"cssnano"
"del"
"gulp"
"gulp-clean"
"gulp-concat"
"gulp-imagemin"
"gulp-postcss"
"gulp-purgecss"
"gulp-rename"
"gulp-sass"
"gulp-sourcemaps"
"gulp-uglify"
"sass"

Scss:
-Variables
-Mixins
-Extend
-Parent Selector

BEM



# Members of the project:

- Mykhailo Starosud;
- Vladyslav Varvarskyi;

# Tasks of the participants:

Mykhailo Starosud: - Gulp settings; - Header of the page; - People Are Talking About Fork section;

Vladyslav Varvarskyi: - Revolutionary Editor section; - Here is what you get section; - Fork Subscription Pricing section; - Project placement at GitHub pages;

# GitHub Pages link: https://varvarskyi.github.io/forkio-project/
